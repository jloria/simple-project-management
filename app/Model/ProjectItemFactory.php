<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 4/24/14
 * Time: 2:43 PM
 */

App::uses('Project','Model');
App::uses('Phase','Model');
App::uses('Task','Model');

class ProjectItemFactory {



    static function getModelById($id,$type)
    {
        $class = self::createProjectItem($type);

        return $class->find('first',$id);
    }


    /**
     * @param $name
     * @return null|ProjectsItem
     * @throws Exception
     */
    static public function createProjectItem($name,$internal_id= null)
    {
        $instanceClass = null;

        switch($name)
        {
            case 'project':
                $instanceClass = new Project();
                break;
            case 'phase':
                $instanceClass = new Phase();
                break;
            case 'task':
                $instanceClass = new Task();
                break;
            default:
                throw new Exception('Class '. $name .' not found');
                break;
        }
        $instanceClass->__internal_id = $internal_id;
        return $instanceClass;
    }

    static public function objectifyResult($params)
    {
        $object = self::createProjectItem($params['ProjectsItem']['type']);
        $object->create($params);
//        var_dump($object);die;
        return $object;
    }


} 