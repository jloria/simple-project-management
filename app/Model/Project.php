<?php

App::uses('ProjectsItem','Model');

/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 4/24/14
 * Time: 9:53 AM
 */

class Project extends ProjectsItem
{

    public $useTable = 'projects_items';

    public function getAll()
    {
        // TODO: Implement getAll() method.
        $params = array( 'conditions' => array( 'Project.type' => 'project', 'Project.parent_id is null') );
        $all = $this->find('all',$params);
        return $all;
    }


    public function canBeAdded()
    {
        return false;
    }

    public function addItem($projectItem)
    {
        $ownType = $this->data['Project']['ProjectsItem']['project_type'];
        $permittedClass = '';
        if($ownType == 'simple')
        {
            $permittedClass = 'Task';
        }elseif($ownType == 'complex'){
            $permittedClass = 'Phase';
        }

        $classToAdd = get_class($projectItem);
        if( $classToAdd != $permittedClass)
        {
            throw new Exception( ucfirst($ownType) . ' projects ' . $this->project_type . ' can\'t add a ' . $classToAdd );
        }

    }

    public function  getResponsible()
    {
        $id = $this->__internal_id;
        $params = array(
            'conditions' => array('Project.id' => $id)
        );

        /** @var Project $project */
        $project = $this->find('first',$params);

        $userName = $project['User']['name'];

        if( is_null($userName) )
        {
            $pI = new ProjectsItem();
            $paramsParent = array(
                'conditions' => array('ProjectsItem.id' => $project['ParentProjectsItem']['id'])
            );
            $valuesParent = $pI->find('first',$paramsParent);

            $objectParent = ProjectItemFactory::createProjectItem($valuesParent['ProjectsItem']['type'],$project['ParentProjectsItem']['id']);

            $userName = $objectParent->getResponsible($project['ParentProjectsItem']['id']);

        }

        return $userName;
    }


}