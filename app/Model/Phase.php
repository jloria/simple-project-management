<?php
App::uses('ProjectsItem','Model');
/**
 * Class for inserting phases in database
 * User: jorge
 * Date: 4/24/14
 * Time: 9:45 AM
 */

class Phase extends ProjectsItem {
    public $useTable = 'projects_items';

    public function addItem($projectItem)
    {
        $permittedClass = 'Task';

        $classToAdd = get_class($projectItem);
        if( $classToAdd != $permittedClass)
        {
            throw new Exception( ' Phase can\'t add a ' . $classToAdd );
        }
    }

    public function  getResponsible()
    {
        $id = $this->__internal_id;
        $params = array(
            'conditions' => array('Phase.id' => $id)
        );

        /** @var Phase $phase */
        $phase = $this->find('first',$params);

        $userName = $phase['User']['name'];

        if( is_null($userName) )
        {
            $pI = new ProjectsItem();
            $paramsParent = array(
                'conditions' => array('ProjectsItem.id' => $phase['ParentProjectsItem']['id'])
            );
            $valuesParent = $pI->find('first',$paramsParent);

            $objectParent = ProjectItemFactory::createProjectItem($valuesParent['ProjectsItem']['type'],$phase['ParentProjectsItem']['id']);

            $userName = $objectParent->getResponsible();

        }

        return $userName;
    }

} 