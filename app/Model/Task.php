<?php
App::uses('ProjectsItem','Model');
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 4/24/14
 * Time: 2:46 PM
 */

class Task extends ProjectsItem{
    public $useTable = 'projects_items';

    public function addItem($projectItem)
    {
        throw new Exception('Task can\'t add objects of class ' . get_class($projectItem) );
    }

    public function  getResponsible()
    {
        $id = $this->__internal_id;
        $params = array(
            'conditions' => array('Task.id' => $id)
        );

        /** @var Task $task */
        $task = $this->find('first',$params);

        $userName = $task['User']['name'];

        if( is_null($userName) )
        {
            $pI = new ProjectsItem();
            $paramsParent = array(
                'conditions' => array('ProjectsItem.id' => $task['ParentProjectsItem']['id'])
            );
            $valuesParent = $pI->find('first',$paramsParent);

            $objectParent = ProjectItemFactory::createProjectItem($valuesParent['ProjectsItem']['type'],$task['ParentProjectsItem']['id']);

            $userName = $objectParent->getResponsible();

        }

        return $userName;

    }

} 