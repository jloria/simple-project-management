<div class="projectsItems view">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __( ucfirst( $projectsItem['ProjectsItem']['type'] ) . ' ' . $projectsItem['ProjectsItem']['name']  ); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3">
            <div class="actions">
                <div class="panel panel-default">
                    <div class="panel-heading">Actions</div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>&nbsp&nbsp;Edit'), array('action' => 'edit', $projectsItem['ProjectsItem']['id']), array('escape' => false)); ?> </li>
                            <li><?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Delete'), array('action' => 'delete', $projectsItem['ProjectsItem']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $projectsItem['ProjectsItem']['id'])); ?> </li>
<!--                            <li>--><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp&nbsp;List Projects Items'), array('action' => 'index'), array('escape' => false)); ?><!-- </li>-->
<!--                            <li>--><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbsp;New Projects Item'), array('action' => 'add'), array('escape' => false)); ?><!-- </li>-->
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp&nbsp;List Users'), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> </li>
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp&nbsp;New User'), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->

        <div class="col-md-9">
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <tbody>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Necessary Time'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['necessary_time']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Deliverables'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['deliverables']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['description']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Planned End Date'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['planned_end_date']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Real End Date'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['real_end_date']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Start Date'); ?></th>
                    <td>
                        <?php echo h($projectsItem['ProjectsItem']['start_date']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('User'); ?></th>
                    <td>
<!--                        --><?php //echo $this->Html->link($projectsItem['User']['name'], array('controller' => 'users', 'action' => 'view', $projectsItem['User']['id'])); ?>
                        <?php echo $responsible; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Parent'); ?></th>
                    <td>
                        <?php echo $this->Html->link($projectsItem['ParentProjectsItem']['name'], array('controller' => 'projects_items', 'action' => 'view', $projectsItem['ParentProjectsItem']['id'])); ?>
                        &nbsp;
                    </td>
                </tr>
<!--                <tr>-->
<!--                    <th>--><?php //echo __('Type'); ?><!--</th>-->
<!--                    <td>-->
<!--                        --><?php //echo h($projectsItem['ProjectsItem']['type']); ?>
<!--                        &nbsp;-->
<!--                    </td>-->
<!--                </tr>-->

                <?php if ( 'project' == $projectsItem['ProjectsItem']['type']): ?>
                    <tr>
                        <th><?php echo __('Project Type'); ?></th>
                        <td>
                            <?php echo h( ucfirst( $projectsItem['ProjectsItem']['project_type'] ) ); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif ?>

                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>

<div class="related row">
    <div class="col-md-12">
        <?php if( $projectsItem['ProjectsItem']['type'] == 'project' ): ?>
            <?php if( $projectsItem['ProjectsItem']['project_type'] == 'simple' ): ?>
                <h3><?php echo __('Related Tasks'); ?></h3>
            <?php endif; ?>
            <?php if( $projectsItem['ProjectsItem']['project_type'] == 'complex' ): ?>
                <h3><?php echo __('Related Phases'); ?></h3>
            <?php endif; ?>
        <?php endif; ?>

        <?php if( $projectsItem['ProjectsItem']['type'] == 'phase' ): ?>
            <h3><?php echo __('Related Tasks'); ?></h3>
        <?php endif; ?>


        <?php if (!empty($projectsItem['ChildProjectsItem'])): ?>
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <th><?php echo __('Necessary Time'); ?></th>
                    <th><?php echo __('Deliverables'); ?></th>
                    <th><?php echo __('Description'); ?></th>
                    <th><?php echo __('Planned End Date'); ?></th>
                    <th><?php echo __('Real End Date'); ?></th>
                    <th><?php echo __('Start Date'); ?></th>
<!--                    <th>--><?php //echo __('Responsible'); ?><!--</th>-->
                    <th class="actions"></th>
                </tr>
                <thead>
                <tbody>
                <?php foreach ($projectsItem['ChildProjectsItem'] as $childProjectsItem): ?>
                    <tr>
                        <td><?php echo $childProjectsItem['name']; ?></td>
                        <td><?php echo $childProjectsItem['necessary_time']; ?></td>
                        <td><?php echo $childProjectsItem['deliverables']; ?></td>
                        <td><?php echo $childProjectsItem['description']; ?></td>
                        <td><?php echo $childProjectsItem['planned_end_date']; ?></td>
                        <td><?php echo $childProjectsItem['real_end_date']; ?></td>
                        <td><?php echo $childProjectsItem['start_date']; ?></td>
<!--                        <td>-->
<!--                            --><?php //echo $childResponsibles[ $childProjectsItem['name'] ]; ?>
<!--                        </td>-->
                        <td class="actions">
                            <?php echo $this->Html->link(__('<span class="glyphicon glyphicon-search"></span>'), array('controller' => 'projects_items', 'action' => 'view', $childProjectsItem['id']), array('escape' => false)); ?>
                            <?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>'), array('controller' => 'projects_items', 'action' => 'edit', $childProjectsItem['id']), array('escape' => false)); ?>
                            <?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span>'), array('controller' => 'projects_items', 'action' => 'delete', $childProjectsItem['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $childProjectsItem['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

        <div class="actions">
            <?php if($projectsItem['ProjectsItem']['type'] == 'project'): ?>
                <?php if($projectsItem['ProjectsItem']['project_type'] == 'simple' ): ?>
                    <?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Task'), array('controller' => 'projects_items', 'action' => 'add', 'task','null', $projectsItem['ProjectsItem']['id']), array('escape' => false, 'class' => 'btn btn-default')); ?>
                <?php endif; ?>

                <?php if($projectsItem['ProjectsItem']['project_type'] == 'complex' ): ?>
                    <?php
                    echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Phase')
                        , array('controller' => 'projects_items', 'action' => 'add', 'phase','null', $projectsItem['ProjectsItem']['id']), array('escape' => false, 'class' => 'btn btn-default'));
                    ?>
                <?php endif; ?>
            <?php endif; ?>

            <?php if($projectsItem['ProjectsItem']['type'] == 'phase' ): ?>
                <?php
                echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Task')
                    , array('controller' => 'projects_items', 'action' => 'add', 'task','null', $projectsItem['ProjectsItem']['id']), array('escape' => false, 'class' => 'btn btn-default'));
                ?>
            <?php endif; ?>

<!--            --><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Child Projects Item'), array('controller' => 'projects_items', 'action' => 'add'), array('escape' => false, 'class' => 'btn btn-default')); ?>
        </div>
    </div>
    <!-- end col md 12 -->
</div>
