<div class="projectsItems form">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Add ' . ucfirst($item_type) ); ?></h1>
			</div>
		</div>
	</div>



	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">

<!--																<li>--><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Projects Items'), array('action' => 'index'), array('escape' => false)); ?><!--</li>-->
									<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Users'), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New User'), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?> </li>
<!--		<li>--><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Projects Items'), array('controller' => 'projects_items', 'action' => 'index'), array('escape' => false)); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Parent Projects Item'), array('controller' => 'projects_items', 'action' => 'add'), array('escape' => false)); ?><!-- </li>-->
							</ul>
						</div>
					</div>
				</div>			
		</div><!-- end col md 3 -->
		<div class="col-md-9">
<!--			--><?php //echo $this->Form->create('ProjectsItem', array('role' => 'form')); ?>
			<?php echo $this->Form->create(ucfirst($item_type), array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('necessary_time', array('class' => 'form-control', 'placeholder' => 'Necessary Time'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('deliverables', array('class' => 'form-control', 'placeholder' => 'Deliverables'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('description', array('class' => 'form-control', 'placeholder' => 'Description'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('planned_end_date', array( 'class' => 'form-control', 'placeholder' => 'Planned End Date'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('real_end_date', array('class' => 'form-control', 'placeholder' => 'Real End Date'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('start_date', array('class' => 'form-control', 'placeholder' => 'Start Date'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('user_id', array('empty' => 'Choose user', 'class' => 'form-control', 'placeholder' => 'User Id'));?>
				</div>
                <?php if($hasParent): ?>
				<div class="form-group">
					<?php echo $this->Form->hidden('parent_id', array('value'=>$parent_id,'class' => 'form-control', 'placeholder' => 'Parent Id'));?>
				</div>
                <?php endif; ?>
				<div class="form-group">
					<?php echo $this->Form->hidden('type', array( 'value'=>$item_type, 'class' => 'form-control', 'placeholder' => 'Type'));?>
					<?php echo $this->Form->hidden('project_type', array( 'value'=>$p_type, 'class' => 'form-control', 'placeholder' => 'Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

		</div><!-- end col md 12 -->
	</div><!-- end row -->
</div>
