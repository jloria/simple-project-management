<div class="projectsItems index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Projects'); ?></h1>
            </div>
        </div><!-- end col md 12 -->
    </div><!-- end row -->



    <div class="row">

        <div class="col-md-3">
            <div class="actions">
                <div class="panel panel-default">
                    <div class="panel-heading">Actions</div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New simple project'), array('controller' => 'ProjectsItems', 'action' => 'add' , 'project', 'simple', null ), array('escape' => false)); ?></li>
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New complex project'), array('controller' => 'ProjectsItems', 'action' => 'add' , 'project', 'complex', null ), array('escape' => false)); ?></li>
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Users'), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> </li>
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New User'), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?> </li>
                        </ul>
                    </div><!-- end body -->
                </div><!-- end panel -->
            </div><!-- end actions -->
        </div><!-- end col md 3 -->

        <div class="col-md-9">
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Necessary time</th>
                    <th>Deliverables</th>
                    <th>Description</th>
                    <th>Planned End Date</th>
                    <th>Real End Date</th>
                    <th>Start Date</th>
                    <th>User</th>
                    <th>Type</th>
                    <th class="actions"></th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($projects)>0): ?>
                <?php foreach ($projects as $projectsItem): ?>
                    <tr>
                        <td><?php echo h($projectsItem['Project']['name']); ?>&nbsp;</td>
                        <td><?php echo h($projectsItem['Project']['necessary_time']); ?>&nbsp;</td>
                        <td><?php echo h($projectsItem['Project']['deliverables']); ?>&nbsp;</td>
                        <td><?php echo h($projectsItem['Project']['description']); ?>&nbsp;</td>
                        <td><?php echo h($projectsItem['Project']['planned_end_date']); ?>&nbsp;</td>
                        <td><?php echo h($projectsItem['Project']['real_end_date']); ?>&nbsp;</td>
                        <td><?php echo h($projectsItem['Project']['start_date']); ?>&nbsp;</td>
                        <td>
                            <?php echo $this->Html->link($projectsItem['User']['name'], array('controller' => 'users', 'action' => 'view', $projectsItem['User']['id'])); ?>
                        </td>
                        <td><?php echo h($projectsItem['Project']['project_type']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array( 'controller' => 'ProjectsItems', 'action' => 'view', $projectsItem['Project']['id']), array('escape' => false)); ?>
                            <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $projectsItem['Project']['id']), array('escape' => false)); ?>

                            <?php
//                            echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>',
//                                array('controller' => 'ProjectsItems','action' => 'viewTree', $projectsItem['Project']['id']),
//                                    array('escape' => false)
//                            ); ?>

                            <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $projectsItem['Project']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $projectsItem['Project']['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php else: ?>
                    <tr> <td colspan="10" >There's no projects</td> </tr>
                <?php endif; ?>
                </tbody>
            </table>


        </div> <!-- end col md 9 -->
    </div><!-- end row -->


</div><!-- end containing of content -->