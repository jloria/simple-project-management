<?php
App::uses('ProjectsItem', 'Model');

/**
 * ProjectsItem Test Case
 *
 */
class ProjectsItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.projects_item',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectsItem = ClassRegistry::init('ProjectsItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectsItem);

		parent::tearDown();
	}

}
