<?php
App::uses('AppController', 'Controller');

App::uses('Project','Model');

/**
 * Home Controller
 *
 */
class HomeController extends AppController {

    public $uses = array('Project');

    public function index()
    {
        $projects = $this->Project->getAll();
        $this->set('projects',$projects);
    }

}
