<?php
App::uses('AppController', 'Controller');
App::uses('ProjectItemFactory','Model');
/**
 * ProjectsItems Controller
 *
 * @property ProjectsItem $ProjectsItem
 * @property PaginatorComponent $Paginator
 */
class ProjectsItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public $helpers = array('Tools.Tree');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectsItem->recursive = 0;
		$this->set('projectsItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectsItem->exists($id)) {
			throw new NotFoundException(__('Invalid projects item'));
		}
		$options = array('conditions' => array('ProjectsItem.' . $this->ProjectsItem->primaryKey => $id));
        $projectItem = $this->ProjectsItem->find('first', $options);

        $object = ProjectItemFactory::createProjectItem($projectItem['ProjectsItem']['type'],$id);


        $responsible = $object->getResponsible()?:'--';


        // a las 9 en el 105
		$this->set('projectsItem', $projectItem );
//		$this->set('childResponsibles', $childResponsibles );
		$this->set('childResponsibles', array() );
		$this->set('responsible', $responsible);

	}

/**
 * add method
 * @param $item_type string type of item to be stored in db
 * @param $p_type string type of project if it is a project to be stored
 * @param $parent_id int|null parent id of item to be created
 * @return void
 */
	public function add($item_type,$p_type,$parent_id = null) {
		if ($this->request->is('post')) {

            /* @var $objectToSave ProjectsItem */
            $objectToSave = ProjectItemFactory::createProjectItem($item_type);
            $objectToSave->create();

            /* @var $parentObject ProjectsItem */
            $parentObjectParams = $this->ProjectsItem->findById($parent_id);
            //throws exception of cannot be added
            if (!empty($parentObjectParams) )
            {
                $parentObject = ProjectItemFactory::objectifyResult($parentObjectParams);
                $parentObject->addItem($objectToSave);
            }

			if ($objectToSave->save($this->request->data)) {
				$this->Session->setFlash(__('The '. ucfirst($item_type) .' has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'view', $objectToSave->getLastInsertID() ));
			} else {
				$this->Session->setFlash(__('The '. ucfirst($item_type) .' could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$users = $this->ProjectsItem->User->find('list');

        $hasParent = ($item_type != 'project');//only tasks and phases has parent

		$this->set(compact('users', 'hasParent','item_type','p_type','parent_id'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectsItem->exists($id)) {
			throw new NotFoundException(__('Invalid projects item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectsItem->save($this->request->data)) {
				$this->Session->setFlash(__('The item has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The item could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('ProjectsItem.' . $this->ProjectsItem->primaryKey => $id));
			$this->request->data = $this->ProjectsItem->find('first', $options);
		}
		$users = $this->ProjectsItem->User->find('list');
		$parentProjectsItems = $this->ProjectsItem->ParentProjectsItem->find('list');
		$this->set(compact('users', 'parentProjectsItems'));
	}

    public function viewTree($id)
    {
        if (!$this->ProjectsItem->exists($id)) {
            throw new NotFoundException(__('Invalid projects item'));
        }
        $options = array('conditions' => array('ProjectsItem.' . $this->ProjectsItem->primaryKey => $id));

        $this->set('projectTree', $this->ProjectsItem->find('threaded', $options));
    }


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectsItem->id = $id;
		if (!$this->ProjectsItem->exists()) {
			throw new NotFoundException(__('Invalid projects item'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProjectsItem->delete()) {
			$this->Session->setFlash(__('The projects item has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The projects item could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
